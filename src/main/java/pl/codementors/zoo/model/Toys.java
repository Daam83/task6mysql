package pl.codementors.zoo.model;

import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString

/**
 * Created by student on 30.06.17.
 */
public class Toys {
    private int id;

    private String kind;
    private String color;
    private  Animal animal;

    public Toys(String kind, String color, Animal animal){
        this.kind = kind;
        this.color = color;
        this.animal= animal;
    }
}
