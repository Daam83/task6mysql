package pl.codementors.zoo.menu;

import pl.codementors.zoo.database.AnimalsDAO;
import pl.codementors.zoo.database.ToysDAO;
import pl.codementors.zoo.model.Animal;
import pl.codementors.zoo.model.Toys;

import java.util.Scanner;

/**
 * Created by student on 30.06.17.
 */
public class ToysManager extends BaseManager<Toys, ToysDAO> {
    public ToysManager() {
        dao =new  ToysDAO();
    }
    @Override
    protected Toys parseNew(Scanner scanner) {
        System.out.println("new kind:");
        String kind = scanner.next();
        System.out.println("new color:");
        String color = scanner.next();
        System.out.println("new animal name:");
        String animalName = scanner.next();
        Animal animal = new AnimalsDAO().findByName(animalName);

        return new Toys(kind,color,animal);
    }

    @Override
    protected void copyId(Toys from, Toys to) { to.setId(from.getId());

    }
}
