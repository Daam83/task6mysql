package pl.codementors.zoo.menu;

import pl.codementors.zoo.database.AnimalsDAO;
import pl.codementors.zoo.database.BreedersDAO;
import pl.codementors.zoo.database.EnclosuresDAO;
import pl.codementors.zoo.database.SpeciesDAO;
import pl.codementors.zoo.model.Animal;
import pl.codementors.zoo.model.Breeder;
import pl.codementors.zoo.model.Enclosure;
import pl.codementors.zoo.model.Species;

import java.util.Scanner;

/**
 * Created by psysiu on 6/30/17.
 */
public class AnimalsManager extends BaseManager<Animal, AnimalsDAO> {

    public AnimalsManager() {
        dao = new AnimalsDAO();
    }

    @Override
    protected Animal parseNew(Scanner scanner) {
        System.out.print("new name: ");
        String name = scanner.next();
        System.out.print("new age: ");
        int age = scanner.nextInt();
        System.out.print("new gender: ");
        String gender = scanner.next();
        System.out.print("new species name: ");
        String speciesName = scanner.next();
        Species species = new SpeciesDAO().findByName(speciesName);
        System.out.print("new breeder name: ");
        String breederName = scanner.next();
        System.out.print("new breeder surname: ");
        String breederSurname = scanner.next();
        Breeder breeder = new BreedersDAO().findByName(breederName, breederSurname);
        System.out.print("new enclosure id: ");
        int enclosureId = scanner.nextInt();
        Enclosure enclosure = new EnclosuresDAO().find(enclosureId);
        return new Animal(name, age, Animal.Gender.valueOf(gender), breeder, species, enclosure);
    }

    @Override
    protected void copyId(Animal from, Animal to) {
        to.setId(to.getId());
    }
}
