package pl.codementors.zoo.menu;

import pl.codementors.zoo.database.BreedersDAO;
import pl.codementors.zoo.database.SpeciesDAO;
import pl.codementors.zoo.model.Breeder;
import pl.codementors.zoo.model.Species;

import java.util.List;
import java.util.Scanner;

/**
 * Created by psysiu on 6/29/17.
 */
public class BreedersManager extends BaseManager<Breeder, BreedersDAO> {

    public BreedersManager() {
        dao = new BreedersDAO();
    }

    @Override
    protected Breeder parseNew(Scanner scanner) {
        System.out.print("new name: ");
        String name = scanner.next();
        System.out.print("new surname: ");
        String surname = scanner.next();
        System.out.print("new position: ");
        String position = scanner.next();
        return  new Breeder(name, surname, Breeder.Position.valueOf(position));
    }

    @Override
    protected void copyId(Breeder from, Breeder to) {
        to.setId(from.getId());
    }

}
