package pl.codementors.zoo.database;

import pl.codementors.zoo.model.Animal;
import pl.codementors.zoo.model.Toys;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by student on 30.06.17.
 */
public class ToysDAO extends BaseDAO<Toys>{
    private String[] columns = {"kind,color,animals"};
    @Override
    public String getTableName() {
        return "toys";
    }

    @Override
    public Toys parseValue(ResultSet result) throws SQLException {
        int id= result.getInt(1);
        String kind = result.getString(2);
        String color = result.getString(3);
        int animalId = result.getInt(4);
        AnimalsDAO dao = new AnimalsDAO();
        Animal animal = dao.find(animalId);
        return new Toys(id,kind,color,animal);
    }

    @Override
    public String[] getColumns() {
        return columns;
    }

    @Override
    public Object[] getColumnsValues(Toys value) {
        Object[] values = {value.getKind(),value.getColor(),value.getAnimal()};
        return  values;
    }

    @Override
    public int getPrimaryKeyValue(Toys value) {
        return value.getId();
    }


}
